package com.android.use.widget;

import java.util.HashSet;
import java.util.Set;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.example.widget.R;
import com.example.widget.WidgetTestActivity;

public class GameWidgetProvider extends AppWidgetProvider {

	public final static String TAG = "GameWidgetProvider";

	private static Set<Integer> idSet = new HashSet<Integer>();

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		if (intent != null) {
			if ("android.appwidget.action.APPWIDGET_UPDATE".equals(intent
					.getAction())) {
				AppWidgetManager appWidgetManager = AppWidgetManager
						.getInstance(context);
				for (int widgetId : idSet) {
					updateAppWidget(context, appWidgetManager, widgetId);
				}
			}
		}
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);

		Log.d(TAG, "onUpdate");
		for (int widgetId : appWidgetIds) {
			idSet.add(widgetId);
			updateAppWidget(context, appWidgetManager, widgetId);
		}
	}

	// 第一个widget被创建时调用
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.d(TAG, "onEnabled");
	}

	// 最后一个widget被删除时调用
	@Override
	public void onDisabled(Context context) {
		Log.d(TAG, "onDisabled");
		super.onDisabled(context);
	}

	private void updateAppWidget(Context context,
			AppWidgetManager appWidgetManager, int widgetId) {
		RemoteViews rv = new RemoteViews(context.getPackageName(),
				R.layout.android_widget_layout);
		rv.setTextViewText(R.id.title,
				context.getString(R.string.widget_game));
		rv.setImageViewResource(R.id.img, R.drawable.widget_game_icon);

		Intent intent = new Intent(context, WidgetTestActivity.class);
		intent.putExtra(WidgetTestActivity.COME_FROM,
				WidgetTestActivity.WIDGET_GAME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pi = PendingIntent.getActivity(context, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		rv.setOnClickPendingIntent(R.id.img, pi);

		appWidgetManager.updateAppWidget(widgetId, rv);
	}
}
