package com.example.widget;

import android.app.Activity;
import android.os.Bundle;

public class WidgetTestActivity extends Activity {

	public static String COME_FROM = "come_from";
	public static int APP = 0 ; 
	public static int WIDGET_TOOL =1; 
	public static int WIDGET_GAME =2; 
	
	private int comeFrom = 0;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		
		initEnterType();
		System.out.println("come from " + comeFrom);
		
	}

	private void initEnterType() {
		comeFrom = getIntent().getIntExtra(COME_FROM, APP);	
	}
	
}